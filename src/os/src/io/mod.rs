pub mod stdio;

pub unsafe fn init() {
    stdio::init();
}
